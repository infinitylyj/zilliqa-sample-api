const { Zilliqa } = require('@zilliqa-js/zilliqa');
const config = require('config')

const zilliqa = new Zilliqa(config.get('api.zil_node'))

module.exports = zilliqa
