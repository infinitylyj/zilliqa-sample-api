const zilliqa = require('./zilliqa-node')
const { BN, Long, bytes, units } = require('@zilliqa-js/util')
const { toBech32Address, fromBech32Address } = require('@zilliqa-js/crypto')
const config = require('config')
const { ownerAddress, ownerPrivateKey } = config.get('wallet')
const CHAIN_ID = 333;
const MSG_VERSION = 1;
const VERSION = bytes.pack(CHAIN_ID, MSG_VERSION);
const myGasPrice = units.toQa('20000', units.Units.Li);
const mygasLimit = Long.fromNumber(10000);
const axios = require('axios');

const cb = (resolve, reject) => (err, data) => {
    const error = err || data.error
    if (error) {
        return reject(error)
    }
    return resolve(data)
}

async function wallet_detail(contract_list, address) {
    let available_token = [];

    await Promise.all(contract_list.map(async (contract) => {
        let smartContractState = await zilliqa.blockchain.getSmartContractState(contract.address);
        if (smartContractState) {
            let info = {
                symbol: contract.symbol,
                name: contract.name,
                amount: 0,
                tokens: [],
            }

            if (contract.type == "ZRC-2") {
                let arrayEntryBalances = Object.entries(smartContractState.result.balances);

                arrayEntryBalances.forEach(x => {
                    if (x[0].toLowerCase() == address.toLowerCase()) {
                        info.amount += parseFloat(x[1]);
                    }
                })
                available_token.push(info);

            }
            if (contract.type == "ZRC-6") {
                let arrayEntryTokenOwner = Object.entries(smartContractState.result.token_owners);
                let arrayEntryTokenUri = Object.entries(smartContractState.result.token_uris);

                await Promise.all(arrayEntryTokenOwner.map(async (x, index) => {
                    if (x[1].toLowerCase() == address.toLowerCase()) {

                        var data = contract.products.filter(x => x.token_id == arrayEntryTokenUri[index][1])[0]
                        var image = data.image_url;
                        info.tokens.push({
                            token_id: arrayEntryTokenUri[index][1],
                            image_url: image
                        })
                        info.amount += 1;
                    }
                }));
                available_token.push(info);
            }
        }
    }));
    return {
        status: "success",
        data: available_token
    };
}

async function token_add(contract_add, user_add) {
    try {
        zilliqa.wallet.addByPrivateKey(ownerPrivateKey);
        const cAddress = contract_add.substring(2);
        const ftAddr = toBech32Address(cAddress);
        const contract = zilliqa.contracts.at(ftAddr);
        const callTx = await contract.call(
            'Transfer',
            [
                {
                    vname: 'to',
                    type: 'ByStr20',
                    value: user_add,
                },
                {
                    vname: 'amount',
                    type: 'Uint128',
                    value: '1',
                }
            ],
            {
                version: VERSION,
                amount: new BN(0),
                gasPrice: myGasPrice,
                gasLimit: mygasLimit,
            }
        );

        var is_success = callTx.receipt.success;

        return {
            status: is_success ? "success" : "failed",
            data: null
        }

    } catch (err) {
        console.log(err);
    }
}

async function token_use(contract_add, user_add) {
    try {
        zilliqa.wallet.addByPrivateKey(ownerPrivateKey);
        const cAddress = contract_add.substring(2);
        const ftAddr = toBech32Address(cAddress);
        const contract = zilliqa.contracts.at(ftAddr);
        const callTx = await contract.call(
            'Receive',
            [
                {
                    vname: 'from',
                    type: 'ByStr20',
                    value: user_add,
                },
                {
                    vname: 'amount',
                    type: 'Uint128',
                    value: '1',
                }
            ],
            {
                version: VERSION,
                amount: new BN(0),
                gasPrice: myGasPrice,
                gasLimit: mygasLimit,
            }
        );

        var is_success = callTx.receipt.success;

        return {
            status: is_success ? "success" : "failed",
            data: null
        }

    } catch (err) {
        console.log(err);
    }
}

async function nft_mint(contract_add, user_add, token_id) {
    try {
        zilliqa.wallet.addByPrivateKey(ownerPrivateKey);
        const cAddress = contract_add.substring(2);
        const ftAddr = toBech32Address(cAddress);
        const contract = zilliqa.contracts.at(ftAddr);
        const callTx = await contract.call(
            'Mint',
            [
                {
                    vname: 'to',
                    type: 'ByStr20',
                    value: user_add,
                },
                {
                    vname: 'token_uri',
                    type: 'String',
                    value: token_id,
                }
            ],
            {
                version: VERSION,
                amount: new BN(0),
                gasPrice: myGasPrice,
                gasLimit: mygasLimit,
            }
        );

        var is_success = callTx.receipt.success;

        return {
            status: is_success ? "success" : "failed",
            data: null
        }

    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    token_add,
    token_use,
    nft_mint,
    wallet_detail
}