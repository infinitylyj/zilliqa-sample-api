
const fs = require('fs')
const { wallet_detail, token_add, token_use, nft_mint } = require('./lib/transaction')
const place = require('../data/place')

module.exports = (app) => {

    //wallet
    app.get('/api/v1/wallet/:id', async (req, res) => {
        try {
            let user_address = req.params.id;
            //get both token information for all smart contract (get smart contract address via place.json) 
            const contract_list = place.filter(x => x.smart_contract_address != null).map(x => {
                return {
                    address: x.smart_contract_address,
                    type: x.smart_contract_type,
                    symbol: x.smart_contract_symbol,
                    name: x.smart_contract_name,
                    base_uri: x.smart_contract_base_uri,
                    products: x.products
                } })
            const response = await wallet_detail(contract_list, user_address)
            res.send(response)
        } catch (err) {
            res.status(500).send({ message: err.toString() })
        }
    })

    //place
    app.get('/api/v1/place/list', async (req, res) => {
        try {

            let response = {
                status: "success",
                data: place
            }
            res.send(response)
        } catch (err) {
            res.status(500).send({ message: err.toString() })
        }
    })

    //zrc-2
    app.post('/api/v1/token/add', async (req, res) => {
        try {
            const { contract_address, user_address } = req.body
            //process transfer token from owner to user address
            //use transfer transition
            const response = await token_add(contract_address, user_address)
            res.send(response)
        } catch (err) {
            res.status(500).send({ message: err.toString() })
        }
    })

    app.post('/api/v1/token/use', async (req, res) => {
        try {
            const { contract_address, user_address } = req.body
            //process transfer token from user to owner address
            //use receive from transition
            const response = await token_use(contract_address, user_address)
            res.send(response)
        } catch (err) {
            res.status(500).send({ message: err.toString() })
        }
    })

    //zrc-6
    app.post('/api/v1/nft/add', async (req, res) => {
        try {
            const { contract_address, user_address, token_id } = req.body
            //process mint nft from smart contract to user address
            //use mint transition
            const response = await nft_mint(contract_address, user_address, token_id)
            res.send(response)
        } catch (err) {
            res.status(500).send({ message: err.toString() })
        }
    })
}